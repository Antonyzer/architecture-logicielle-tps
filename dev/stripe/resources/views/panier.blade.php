<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Mon Panier</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body>
  <div class="container">
    <div class="row">
      <h3>Mon panier</h3>
    </div>
    <div class="row">
      <form method="post" action="payment">
        @csrf

        @foreach ($products as $key => $product)
        <div>
          <label>{{ $product['name'] }} : {{ $product['amount'] }} €</label>
          <input type="number" name="products[{{ $key }}][quantity]" value="0" />
          <input type="hidden" name="products[{{ $key }}][id]" value="{{ $product['id'] }}" />
          <input type="hidden" name="products[{{ $key }}][price_id]" value="{{ $product['price_id'] }}" />
        </div>
        @endforeach

        <button type="submit">Payer</button>
      </form>
    </div>
  </div>
</body>

</html>