<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StripeController extends Controller
{
    /**
     * Display panier
     */
    public function panier()
    {
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        // Get all products and prices from API
        $products = $stripe->products->all();
        $prices = $stripe->prices->all();

        $myProducts = array();

        // Associate products and prices
        foreach ($products['data'] as $product) {
            foreach ($prices['data'] as $price) {
                if ($product['id'] === $price['product'] && $price['active']) {
                    $newProduct = array(
                        'id' => $product['id'],
                        'price_id' => $price['id'],
                        'name' => $product['name'],
                        'amount' => implode(',', str_split($price['unit_amount'], strlen($price['unit_amount']) - 2)),
                    );
                    array_push($myProducts, $newProduct);
                }
            }
        }

        return view('panier', array('products' => $myProducts));
    }

    /**
     * Redirect user to payment page
     */
    public function payment(Request $request)
    {
        $products = $request->all()['products'];
        $items = array();

        foreach ($products as $product) {
            if ($product['quantity'] > 0) {
                $newItem = array(
                    'price' => $product['price_id'],
                    'quantity' => $product['quantity'],
                );
                array_push($items, $newItem);
            }
        }

        if (sizeof($items) > 0) {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            header('Content-Type: application/json');

            $appUrl = env('APP_URL', 'http://localhost:8000/');

            $checkout_session = \Stripe\Checkout\Session::create([
                'line_items' => $items,
                'mode' => 'payment',
                'success_url' => $appUrl . 'success.html',
                'cancel_url' => $appUrl . 'cancel.html',
            ]);

            return redirect($checkout_session->url);
        } else {
            return redirect('/');
        }
    }
}
