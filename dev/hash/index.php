<?php

require_once 'controllers/AuthController.php';

$authController = new AuthController();
$action = $_GET['action'] ?? null;

if (isset($action)) {
    if ($action == 'register') {
        $authController->register();
    }
} else {
    $authController->showRegister();
}
