<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Inscription</title>
</head>

<body>
  <p><?= $error ?? '' ?></p>
  <p>Les champs accompagnés du symbole <strong>*</strong> sont obligatoires.</p>
  <form method="post" action="index.php?action=register">
    <div>
      <label for="username">Identifiant*</label>
      <input type="text" name="username" id="username" required />
    </div>
    <div>
      <label for="password">Mot de passe*</label>
      <input type="text" name="password" id="password" required />
    </div>
    <div>
      <label for="email">Email</label>
      <input type="text" name="email" id="email" />
    </div>
    <div>
      <button type="submit">Enregistrer/Se connecter</button>
    </div>
  </form>
</body>

</html>