<?php

require_once 'models/User.php';

class AuthController
{
    private $user;
    private $pepper;

    public function __construct()
    {
        $this->user = new User();

        $params = parse_ini_file('env.ini');
        $this->pepper = $params['pepper'];
    }

    public function showRegister()
    {
        include 'views/register.php';
    }

    public function register()
    {
        // Get form data
        $user = array(
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'email' => $_POST['email'],
        );

        $existingUser = $this->user->get($user['username']);

        // Check username exists in database
        if (is_array($existingUser)) {
            $user['password'] = crypt($user['password'], $existingUser['SALT']);

            // Check passwords
            if (hash_equals($user['password'], $existingUser['PASSWORD'])) {
                // Decrypt user email to display
                $existingUser['EMAIL'] = openssl_decrypt($existingUser['EMAIL'], 'AES-128-ECB', $this->pepper);
                include 'views/dashboard.php';
            } else {
                $error = 'bad password';
                include 'views/register.php';
            }
        } else {
            // Hash password
            $user['salt'] = $this->generateRandomString();
            $user['password'] = crypt($user['password'], $user['salt']);

            // Crypt email
            $user['email'] = openssl_encrypt($user['email'], 'AES-128-ECB', $this->pepper);

            // Create new user
            $this->user->create($user);

            include 'views/register.php';
        }
    }

    private function generateRandomString()
    {
        $alphabet = "abcdefghijklmnopqrstuvwxyz";

        $shuffledAlphabet = str_shuffle($alphabet);
        $string = substr($shuffledAlphabet, random_int(10, 15));

        return $string;
    }
}
