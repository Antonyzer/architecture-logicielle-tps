<?php

class User
{
    private $db;

    public function __construct()
    {
        $this->db = $this->dbConnect();
    }

    private function dbConnect()
    {
        $params = parse_ini_file('env.ini');

        try {
            $db = new PDO($params['url'], $params['user'], $params['passwd'], array(\PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8'));
            return $db;
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function create($user)
    {
        $sql = "INSERT INTO users(username, password, salt, email) VALUES(?,?,?,?)";
        $stmt = $this->db->prepare($sql);

        $stmt->execute(array($user['username'], $user['password'], $user['salt'], $user['email']));
    }

    public function get($username)
    {
        $sql = "SELECT * FROM users WHERE username = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$username]);

        return $stmt->fetch();
    }
}
