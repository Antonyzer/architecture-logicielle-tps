<?php

require_once './MyPDO.php';

class DatabasePooling
{
  private static $_instance;

  private $_connections;

  private function __construct()
  {
    // Get configuration for database connection
    $params = parse_ini_file('db.ini');

    // Initialize empty array
    $this->_connections = array();

    for ($i = 0; $i < $params['number']; $i++) {
      try {
        $db = new MyPDO($params['url'], $params['user'], $params['passwd'], array(
          \PDO::ATTR_PERSISTENT => true,
          \PDO::ATTR_EMULATE_PREPARES => false,
          \PDO::MYSQL_ATTR_INIT_COMMAND =>  'SET NAMES utf8'
        ));
        $db->busy = false;
        array_push($this->_connections, $db);
      } catch (PDOException $e) {
        array_push($this->_connections, $e);
      }
    }
  }

  public static function getInstance()
  {
    if (is_null(self::$_instance)) {
      self::$_instance = new DatabasePooling();
    }

    return self::$_instance;
  }

  public function getConnections()
  {
    return $this->_connections;
  }

  public function getFirstFreeConnection()
  {
    foreach ($this->_connections as $key => $connection) {
      if (!$connection->busy) {
        $this->_connections[$key]->busy = true;
        return $connection;
      }
    }

    return null;
  }
}
