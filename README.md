# Architecture logicielle - Travaux personnels

Auteur : CASTANER Antony

## Sommaire

- [Architecture logicielle - Travaux personnels](#architecture-logicielle---travaux-personnels)
  - [Sommaire](#sommaire)
  - [1. Architecture logicielle de QuickMS](#1-architecture-logicielle-de-quickms)
  - [2. Maitre de données](#2-maitre-de-données)
  - [3. Interconnexion entre deux étudiants](#3-interconnexion-entre-deux-étudiants)
  - [4. Paiement par Stripe](#4-paiement-par-stripe)
  - [5. Hachage](#5-hachage)
  - [6. Database pooling](#6-database-pooling)

## 1. Architecture logicielle de QuickMS

Pour commencer le module, nous devons représenter l'architecture logicielle de notre entreprise où nous sommes en alternance.

Pour ma part, je travaille chez QuickMS, qui une jeune entreprise Marseille. Nous proposons des outils Saas pour les ressources humaines, les comités sociaux et économiques ainsi que les salariés en général.

![QuickMs-architecture](img/architecture-quickms.png)

## 2. Maitre de données

A la suite de l'architecture précédente, nous devons réaliser une modélisation pour les maîtres et consommateurs de la donnée.

![QuickMS-modelisation-maitre-donnees](img/illustration_maitre_donnees.png)

## 3. Interconnexion entre deux étudiants

Ce TP est détaillé dans un autre document au vu de la longueur du document : [TP Interconnexion](tp-interconnexion-deux-etudiants.md)

## 4. Paiement par Stripe

Un TP consiste en la mise en place d'un moyen de paiement à l'aide de Stripe. J'ai réalisé ce code avec Laravel, il aurait été préférable de le code en natif PHP par exemple vu la simplicité de ce code.

Ce code est situé dans le fichier **/dev/stripe/app/Http/Controllers/StripeController.php**.

```php
    /**
     * Display panier
     */
    public function panier()
    {
        $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
        // Get all products and prices from API
        $products = $stripe->products->all();
        $prices = $stripe->prices->all();

        $myProducts = array();

        // Associate products and prices
        foreach ($products['data'] as $product) {
            foreach ($prices['data'] as $price) {
                if ($product['id'] === $price['product'] && $price['active']) {
                    $newProduct = array(
                        'id' => $product['id'],
                        'price_id' => $price['id'],
                        'name' => $product['name'],
                        'amount' => implode(',', str_split($price['unit_amount'], strlen($price['unit_amount']) - 2)),
                    );
                    array_push($myProducts, $newProduct);
                }
            }
        }

        return view('panier', array('products' => $myProducts));
    }

    /**
     * Redirect user to payment page
     */
    public function payment(Request $request)
    {
        $products = $request->all()['products'];
        $items = array();

        foreach ($products as $product) {
            if ($product['quantity'] > 0) {
                $newItem = array(
                    'price' => $product['price_id'],
                    'quantity' => $product['quantity'],
                );
                array_push($items, $newItem);
            }
        }

        if (sizeof($items) > 0) {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            header('Content-Type: application/json');

            $appUrl = env('APP_URL', 'http://localhost:8000/');

            $checkout_session = \Stripe\Checkout\Session::create([
                'line_items' => $items,
                'mode' => 'payment',
                'success_url' => $appUrl . 'success.html',
                'cancel_url' => $appUrl . 'cancel.html',
            ]);

            return redirect($checkout_session->url);
        } else {
            return redirect('/');
        }
    }
```

Deux pages ont également été créées dans le dossier **/dev/stripe/app/public** afin de gérer les retours en cas d'erreur ou d'annulation du paiement.

Pour accéder au code : [Lien vers le code Stripe](dev/stripe)

## 5. Hachage

Un TP avec Monsieur DEPORT consiste à hacher et déchiffrer les informations personnelles d'un utilisateur. Nous chiffrons les informations personnelles (email ici) lors de l'inscription, le mot de passe est quand à lui hacher à l'aide d'un sel choisi.

Pour accéder au code : [Lien vers le code Hash](dev/hash)

## 6. Database pooling

Un TP avec Monsieur DEPORT consiste à la mise en place d'un gestionnaire de connexions à la base de données. En effet, l'ouverture d'une connexion à une base de données peut être couteuse. Pour éviter cela, nous ouvrons X connexions au démarrage de l'application. Nous utilisons ensuite celles disponibles afin de réaliser des requêtes en direction de notre SGBD.

Pour accéder au code : [Lien vers le code Database pooling](dev/database-pooling)
