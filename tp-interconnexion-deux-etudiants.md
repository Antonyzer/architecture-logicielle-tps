# Interconnexion entre deux étudiants - CASTANER Antony

Suite à mon départ anticipé le 10/12 décembre dernier, j'ai décidé de travailler sur ce TP en solo en utilisant la virtualisation et plus précisemment VMware Workstation afin de réaliser ce travail.

## Infrastructure utilisée

L'infrastructure est composée de deux sites, possédant chacun un routeur PFSense et un PC Windows 10.

![infra_interconnexion](img/tp-interconnexion/infra_interconnexion.png)

Toutes les adressses IP sont statiques parès un paramétrage manuel de chacun des périphériques.

Sur le site A, nous configurons l'IP fixe du PC A avec le DHCP du routeur A grâce à son adresse MAC. Sur PFSense, nous nous rendonds dans **Services** puis **DHCP Server**, il doit être activé.

![pfsense_A_static_IP_win](img/tp-interconnexion/pfsense_A_static_IP_win.png)

Sur le site B, nous configurons l'IP fixe du PC B avec le DHCP du routeur B grâce à son adresse MAC.

![pfsense_B_static_IP_win](img/tp-interconnexion/pfsense_B_static_IP_win.png)

## Dossier partagé depuis le site A

Pour notre activité, nous créons un dossier partagé sur le PC A.

![win10_A_shared_folder](img/tp-interconnexion/win_10_a_shared_folder.png)

## Configuration du routeur

Nous configurons désormais notre routeur sur le site A. Nous nous rendons sur l'interface web du PFSense. Après nous être connecté, nous nous rendons dans **Firewall** puis **Nat**.

Nous ajoutons une nouvelle redirection dans la partie **Port Forward**.

Nous précisons la source comme l'adresse IP "publique" du routeur du site B. La destination est bien le PC dans le site A sur le port 445.

![pfsense_A_nat_port_forward](img/tp-interconnexion/pfsense_A_nat_port_forward.png)

Si nous nous rendons dans la partie **Firewall** puis **Rules**, nous pouvons apercevoir qu'une règle a été créée sur l'interface WAN afin d'autoriser le traffic comme indiqué sur le redirection précédente.

![pfsense_A_firewall_rule](img/tp-interconnexion/pfsense_A_firewall_rule.png)

## Test du dossier partagé sur le site B

Sur le PC B, nous entrons **\\10.13.1.128\partage** dans l'explorateur de fichiers.

![win_10_b_test_connection](img/tp-interconnexion/win_10_b_test_connection.png)

Après avoir entre nos identifiants du PC A, nous accédons au dossier partagé.

![win_10_b_success](img/tp-interconnexion/win_10_b_success.png)

## Création d'un compte local sur le Site A

Nous créons un nouvel utilisateur **MrRobot** dans **Gestion de l'ordinateur**.

![win_10_a_create_new_user](img/tp-interconnexion/win_10_a_create_new_user.png)

Nous testons l'utilisateur à l'aide de la commande ci-dessous.

![win_10_a_test_mr_robot_local](img/tp-interconnexion/win_10_a_test_mr_robot_local.png)

## Ajout du nouveau compte local au dossier partagé

Nous ajouton **MrRobot** au dossier **partage** avec les droits suivants :

- Modification,
- Lecture et exécution,
- Affichage du contenu du dossier,
- Lecture,
- Ecriture.

![win_10_a_add_mrrobot_to_shared_folder](img/tp-interconnexion/win_10_a_add_mrrobot_to_shared_folder.png)

![win_10_a_add_mrrobot_to_shared_folder_autor](img/tp-interconnexion/win_10_a_add_mrrobot_to_shared_folder_autor.png)

## Création et planification du script de synchonisation

Pour notre exemple, nous créons un dossier **MesCours** dans mes documents, qui comportent des fichiers texte faisant office de notes de cours. Nous créons le script suivant **copyLocalToSharedFolder.ps1** :

```powershell
Robocopy C:\Users\Antony-B\Documents\MesCours \\10.13.1.128\partage\MesCours
```

Afin de pouvoir l'exécuter, nous devons modifier la police d'exécution qui est à **Restricted** par défaut.

![win_10_b_update_script_rights](img/tp-interconnexion/win_10_b_update_script_rights.png)

Nous ouvrons le planificateur de tâches afin de créer une nouvelle tâche planifiée que nous nommons **Synchronisation des cours**.

![win_10_b_task_scheduler_trigger](img/tp-interconnexion/win_10_b_task_scheduler_trigger.png)

![win_10_b_task_scheduler_command_script](img/tp-interconnexion/win_10_b_task_scheduler_command_script.png)

Après plusieurs ajouts de fichiers et plusieurs synchronisations, le script fonctionne bien et ne retourne pas d'erreur :

![win_10_b_success_synchro](img/tp-interconnexion/win_10_b_success_synchro.png)

## Test de la synchronisation

Les dossiers sur les PC A et B sont bien synchronisés toutes les 5 minutes :

![win_10_b_folder_synch](img/tp-interconnexion/tp-interconnexion/win_10_b_folder_synch.png)

![win_10_a_shared_folder_synch](img/tp-interconnexion/win_10_a_shared_folder_synch.png)

Pour finaliser le test, nous avons ajouté un dernier fichier **Cours4** sur le PC B du Site B.

![win_10_b_new_course_4](img/tp-interconnexion/win_10_b_new_course_4.png)

Après synchronisation quelques minutes plus tard, le fichier est également présent sur le dossier partagé.

![win_10_a_new_course_4](img/tp-interconnexion/win_10_a_new_course_4.png)
